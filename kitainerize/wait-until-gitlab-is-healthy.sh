#!/bin/sh
cd `dirname "$_"`
while ! ./is-gitlab-healthy.sh ; do
  echo "$0: not yet... `date -u`"
  sleep 10
done

# Healthy does not mean ready to handle requests.
# TODO: Probably should try to make web requests until
# we get back a good responce. For now, we'll sleep
# an additional 10 seconds.
sleep 10
