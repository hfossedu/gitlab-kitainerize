#!/bin/sh
cd `dirname "$_"`

# Exit on error
set -e

# Parameters
GITLAB_IMAGE=$1
KITAINER_IMAGE=$2
FILE="$3"
PROJECT=$4

# Exit if we don't have all our parameters.
if [ -z "$FILE" ] || [ -z $PROJECT ] || [ -z $GITLAB_IMAGE ] || [ -z $KITAINER_IMAGE ] ; then
  echo "usage: $0 GITLAB_IMAGE KITAINER_IMAGE FILE PROJECT"
  exit 1
fi

# Constants
GITLAB_CONTAINER=gitlab
GITLAB_ROOT_PASSWORD=rootroot
HOST_NAME=localhost
OWNER00_NAME=owner00
OWNER00_PASSWORD=owner00owner00
TOKEN_NAME=kitainer-token

echo "STARTING GITLAB $GITLAB_IMAGE"
docker run \
    --rm \
    --detach \
    --name $GITLAB_CONTAINER \
    --env GITLAB_ROOT_PASSWORD=$GITLAB_ROOT_PASSWORD \
    --hostname $HOST_NAME \
    --publish 443:443 --publish 80:80 --publish 22:22 \
    $GITLAB_IMAGE

echo "WAITING UNTIL GITLAB IS HEALTHY"
./wait-until-gitlab-is-healthy.sh

echo "GETTING ROOT TOKEN"
TOKEN=`./get-token.py http://$HOST_NAME/ root rootroot kitainer-token`
echo "$TOKEN" > ./gitlab-kitainer/image-files/tokens/root

echo "ADDING OWNER00"
./gitlab-kitainer/image-files/bin/create-user.sh $OWNER00_NAME $OWNER00_PASSWORD

echo "GETTING $OWNER00_NAME TOKEN"
OWNER00_TOKEN=`./get-token.py http://$HOST_NAME/ $OWNER00_NAME $OWNER00_PASSWORD $TOKEN_NAME`
echo "$OWNER00_TOKEN" > ./gitlab-kitainer/image-files/tokens/owner00

echo "IMPORTING PROJECT $FILE AS $PROJECT"
OUTPUT=`curl -s --request POST --header "PRIVATE-TOKEN: $OWNER00_TOKEN" --form "file=@$FILE" --form "path=$PROJECT" "http://$HOST_NAME/api/v4/projects/import"`

echo "GETTING PROJECT ID from $OUTPUT"
PROJECT_ID=`./extract-id.py "$OUTPUT"`
# PROJECT_ID=`echo $OUTPUT | perl -nle'print $& while m{(?<="id":)[0-9]+}g'`

echo "WAITING UNTIL PROJECT IS IMPORTED"
./wait-until-import-is-finished.sh "$OWNER00_TOKEN" "$PROJECT_ID" "$HOST_NAME"

echo "EXTRACTING DATA"
./extract-data.sh $GITLAB_CONTAINER

echo "STOPPING AND REMOVING GITLAB"
docker stop $GITLAB_CONTAINER

echo "BUILD KITAINER"
docker build \
    --build-arg GITLAB_IMAGE=$GITLAB_IMAGE \
    --tag $KITAINER_IMAGE \
    ./gitlab-kitainer/
