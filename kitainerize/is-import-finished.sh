#!/bin/sh
TOKEN="${1}"
PROJECT_ID="${2}"
HOST_NAME="${3}"
echo `curl -s --request GET --header "PRIVATE_TOKEN: ${1}" "http://$HOST_NAME/api/v4/projects/${PROJECT_ID}/import"` | grep finished
