#!/bin/sh
cd `dirname "$_"`

set -e
set -x

if [ ! -e /opt/gitlab-kitainer/var/is-not-first-run ] ; then
    echo "$0: COPYING GITLAB DATA INTO POSITION"
    mkdir -p /etc/ /var/log/ /var/opt/
    cd /
    tar --same-owner -zxpvf /opt/gitlab-kitainer/gitlab-data/etc.tgz
    tar --same-owner -zxpvf /opt/gitlab-kitainer/gitlab-data/log.tgz
    tar --same-owner -zxpvf /opt/gitlab-kitainer/gitlab-data/opt.tgz

    mkdir -p /opt/gitlab-kitainer/var
    touch /opt/gitlab-kitainer/var/is-not-first-run
fi

## If we are deployed using AWS ECS, set GitLab's externa_url to
## the exteranl IP address.
if command -v aws ; then
    EXTERNAL_IP=`aws ecs list-tasks --query "taskArns" --output text | xargs aws ecs describe-tasks --query "tasks[].taskArn,taskDefinitionArn,containerInstanceArn" --tasks`
    echo "EXTERNAL_IP is $EXTERNAL_IP"
    sed -i "s/external_url 'GENERATED_EXTERNAL_URL'/external_url 'http://$EXTERNAL_IP'/g" /etc/gitlab/gitlab.rb
fi

echo "$0: STARTING GITLAB"
/assets/wrapper
