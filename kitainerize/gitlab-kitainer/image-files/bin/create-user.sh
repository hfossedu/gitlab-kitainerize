#!/bin/sh
cd `dirname "$_"`

set -e

curl \
  --request POST \
  --header "PRIVATE-TOKEN: $(cat ../tokens/root)" \
  --header "Content-Type: application/json" \
  --data '{"email":"'$1'@bogus.hfoss.org", "username":"'$1'", "name":"'$1'", "password":"'$2'", "skip_confirmation":true}' \
  "http://localhost/api/v4/users"
