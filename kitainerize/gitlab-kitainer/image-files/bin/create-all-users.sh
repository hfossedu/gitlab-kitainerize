#!/bin/sh
cd `dirname "$_"`

set -e
# set -x

while read NAME PASS REMAINDER; do
    OUT=`./create-user.sh "$NAME" "$PASS"`
    ID=`echo $OUT | perl -nle'print $& while m{(?<="id":)[0-9]+}g'`
    echo "$ID $NAME $PASS $REMAINDER"
done