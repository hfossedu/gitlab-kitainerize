#!/bin/sh
cd `dirname "$_"`

TOKEN="$1"
PROJECT_ID="$2"
HOST_NAME="$3"

while ! ./is-import-finished.sh "${TOKEN}" "${PROJECT_ID}" "${HOST_NAME}"; do
  echo "not yet... `date -u`"
  sleep 10
done
