#! /usr/bin/env python3
'''
MIT License

Copyright (c) 2019 Kelly Hair

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

import sys
import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

gitlab_URL = sys.argv[1]
username = sys.argv[2]
password = sys.argv[3]
api_key_name = sys.argv[4]

# instantiate a chrome options object so you can set the size and headless preference
chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument("--window-size=1920x1080")

# download the chrome driver from https://sites.google.com/a/chromium.org/chromedriver/downloads and put it in the
# current directory
# on Mac OS X(what's used here..) you can use brew: `brew cask install chromedriver`
chrome_driver = "/usr/local/bin/chromedriver"

driver = webdriver.Chrome(options=chrome_options, executable_path=chrome_driver)

# Wait up to 10 seconds when trying to find an element.
driver.implicitly_wait(30)

driver.get(f"{gitlab_URL}/profile/personal_access_tokens")

driver.find_element_by_id("user_login").send_keys(f"{username}")
driver.find_element_by_id ("user_password").send_keys(f"{password}")
driver.find_element_by_name("commit").click()

driver.find_element_by_id("personal_access_token_name").send_keys(f"{api_key_name}")
driver.find_element_by_xpath("//form[@id='new_personal_access_token']/div[3]/fieldset").click()
driver.find_element_by_id("personal_access_token_scopes_api").click()
driver.find_element_by_xpath("//form[@id='new_personal_access_token']/div[3]/fieldset[2]").click()
driver.find_element_by_id("personal_access_token_scopes_read_user").click()
driver.find_element_by_id("personal_access_token_scopes_read_repository").click()
driver.find_element_by_id("personal_access_token_scopes_write_repository").click()
if username == 'root':
    driver.find_element_by_id("personal_access_token_scopes_sudo").click()
driver.find_element_by_name("commit").click()
token = driver.find_element_by_name("created-personal-access-token").get_attribute('value')
print(token)
