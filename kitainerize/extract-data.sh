#!/bin/sh
cd `dirname "$_"`

GITLAB_CONTAINER=$1

rm -rf ./gitlab-kitainer/image-files/gitlab-data/*
touch ./gitlab-kitainer/image-files/gitlab-data/.gitkeep

docker exec -it $GITLAB_CONTAINER rm -f /tmp/etc.tgz /tmp/log.tgz /tmp/opt.tgz
docker exec -it $GITLAB_CONTAINER tar -zvcpf /tmp/etc.tgz /etc/gitlab
docker exec -it $GITLAB_CONTAINER tar -zvcpf /tmp/log.tgz /var/log/gitlab
docker exec -it $GITLAB_CONTAINER tar -zvcpf /tmp/opt.tgz /var/opt/gitlab

docker cp $GITLAB_CONTAINER:/tmp/etc.tgz ./gitlab-kitainer/image-files/gitlab-data/
docker cp $GITLAB_CONTAINER:/tmp/log.tgz ./gitlab-kitainer/image-files/gitlab-data/
docker cp $GITLAB_CONTAINER:/tmp/opt.tgz ./gitlab-kitainer/image-files/gitlab-data/
